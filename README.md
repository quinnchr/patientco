This implements a CSV to JSON converter. The web app can be tested at:
https://csv.quinnchrzan.com

The CSV parser supports the following
format:

* Rows are newline delimited
* Columns are pipe or comma delimited
* Fields support any ASCII character
* Fields can be quoted with double quotes (e.g. "My, Field")
* Quoted fields can contain escaped double quotes (e.g. "The ""Title""")

Note: By default the parser uses a strict mode where trailing newline will be
treated as an empty row and field data cannot have quoted and unquoted data in
a single field, e.g. `"this is a "field` is invalid.

# Usage

There's three ways to use the csv parser:

1. As a library

```
import { csv } from './parse.js';

let data = `header1, header2, header3\nfield1, field2, field3`;

// initiate parser with strict mode
let parser = new csv();

// initiate parser without strict mode
parser = new csv(false);

let parsed = parser.parse(data, false);
```

2. As a command line utility

```
./convert.js test/test.csv
```

3. As a web app

```
npm start
```

# Architecture

The parser itself is written as a state machine which does two passes, rows then
columns. This isn't the most efficient method and it could be done in a single
pass but it makes testing a bit easier to be able to separate row and column parsing.

# Tests

Tests are implemented via Jest.

```
npm run test
```

# TODO

* The parser needs more tests, CSV parsing has a lot of edge cases
* The web app needs tests as well
