#!/usr/bin/env node

const convert = require('./src/libraries/convert.js');

const cmd = new convert();

if (process.argv.length !== 3) {
    cmd.help();
}

cmd.read(process.argv.pop()).then((data) => {
    console.log(cmd.toJSON(data));
}).catch(err => cmd.error(err.message));
