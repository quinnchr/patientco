import React, { Component } from 'react';
import CSV from './components/csv.js';
import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            file: null,
            list: null
        };
    }

    onDrop(e) {
        e.preventDefault();
        let file;

        if (e.dataTransfer.items) {
            if (e.dataTransfer.items[0].kind === 'file') {
                file = e.dataTransfer.items[0].getAsFile();
            }
        } else {
            file = e.dataTransfer.files[0];
        } 

        this.setState({
            file: file,
            list: e.dataTransfer.files
        });
    }

    render() {
        return (
            <div className="App" onDrop={e => this.onDrop(e)} onDragOver={e => e.preventDefault()}>
                <CSV file={this.state.file} fileList={this.state.list} />
            </div>
       );
    }

}

export default App;
