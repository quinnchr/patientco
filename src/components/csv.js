import React, { Component } from 'react';
import Error from '../components/error.js';
import { csv } from '../libraries/parse.js';

class CSV extends Component {

    constructor(props) {
        super(props);
        this.csv = new csv(false);
        this.state = {
            file: this.props.file,
            loading: false,
            json: null,
            error: null,
            delimiter: null
        };
    }

    /* parses the current file into JSON, it will guess the delimiter if none is provided */
    getJSON(file, delimiter = null) {
        let data, parsed, json, error;
        let reader = new FileReader();

        reader.onload = (e) => {
            try {
                data = e.target.result;

                if ( ! delimiter) {
                    let rows = this.csv.parseRows(data);
                    [delimiter] = this.csv.detect(rows[0]);
                }

                parsed = this.csv.parse(data, delimiter);
                json = JSON.stringify(parsed, null, 2);
            } catch (e) {
                error = e.message;
            }

            this.setState({
                loading: false,
                json: json,
                error: error,
                delimiter: delimiter
            });
        };

        reader.onloadstart = () => this.setState({ loading: true });
        reader.readAsText(file);
    }

    /* returns the current file name with a csv extension */
    getFileName() {
        if (this.state.file) {
            let original = this.state.file.name;
            let parts = original.split('.');
            parts.pop();

            return parts.join('.') + '.json';
        }
    }

    /* returns a data URI for the parsed JSON */
    getURI() {
        return 'data:application/octet-stream,' + encodeURIComponent(this.state.json);
    }

    onChangeFile(e) {
        if (e.target.files.length > 0) {
            let file = e.target.files[0];

            if (this.state.file !== file) {
                this.getJSON(file);
                this.setState({ file: file });
            }
        }
    }

    onChangeDelimiter(e) {
        // reparse the file with the new delimiter
        this.setState({
            delimiter: e.target.value
        });
        this.getJSON(this.state.file, e.target.value);
    }

    componentDidUpdate(prevProps) {
        // set the file input to match the dropped file
        if (this.props.fileList !== prevProps.fileList) {
            this.refs.input.files = this.props.fileList;
            // we have to manually trigger a change event for Firefox
            let e = new Event('change', { bubbles: true });
            this.refs.input.dispatchEvent(e);
        }
    }

    render() {
        return (
            <div className={`csv ${this.state.json ? 'active' : ''}`}>
                <div className="fileInput">
                    { ! this.state.json &&
                        <p>Choose a CSV file or drag and drop to convert</p>
                    }
                    <input type="file" onChange={e => this.onChangeFile(e)} ref="input" />
                </div>
                {this.state.loading &&
                    <p>Loading...</p>
                } { ! this.state.loading && this.state.json &&
                    <div className="display">
                        <div className="controls">
                            <a download={this.getFileName()} href={this.getURI()}>Download</a>
                            comma
                            <input type="radio"
                                value=","
                                checked={this.state.delimiter === ","}
                                onChange={(e) => this.onChangeDelimiter(e)}/>
                            pipe
                            <input type="radio"
                                value="|"
                                checked={this.state.delimiter === "|"}
                                onChange={(e) => this.onChangeDelimiter(e)}/>
                        </div>
                        <pre>
                            <code>
                                {this.state.json}
                            </code>
                        </pre>
                    </div>
                } {this.state.error &&
                    <Error>{this.state.error}</Error>
                }
            </div>
        );
    }

}

export default CSV;
