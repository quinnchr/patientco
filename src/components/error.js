import React from 'react';

const Error = ({children}) => (
    <div className="error">
        Error: {children}
    </div>
);

export default Error;
