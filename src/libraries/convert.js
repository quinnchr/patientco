// add support for es6 modules
require = require("esm")(module);

const fs = require('fs');
const parse = require('./parse.js');

class convert {

    read(file) {
        return new Promise((resolve, reject) => {
            fs.readFile(file, { encoding: 'ascii' }, (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(data);
                }
            });
        });
    }

    help() {
        console.log(`
            usage: convert.js FILENAME

            Convert a CSV file to JSON`);
    }

    error(msg) {
        console.error(msg);
        process.exit(1);
    }

    toJSON(data) {
        const csv = new parse.csv(false);
        const parsed = csv.parse(data);
        return JSON.stringify(parsed);
    }

}

module.exports = convert;
