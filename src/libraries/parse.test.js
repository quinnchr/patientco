import { csv } from './parse.js';
import { stripIndents } from 'common-tags'

const parser = new csv(false);
const strict = new csv();

/* Row parsing tests */

test('Parses newline delimited rows', () => {
    let data = stripIndents`
        row1
        row2
        row3
        row4
    `;
    let rows = parser.parseRows(data);
    expect(rows).toEqual(['row1', 'row2', 'row3', 'row4']);
});

test('Parses rows containing quoted newlines', () => {
    let data = stripIndents`
        row1
        "row2
        continued"
        row3
        row4
    `;
    let rows = parser.parseRows(data);
    expect(rows).toEqual(['row1', '"row2\ncontinued"', 'row3', 'row4']);
});

test('Parses trailing newline', () => {
    let data = `row1\nrow2\nrow3\nrow4\n`;
    let rows = parser.parseRows(data);
    expect(rows).toEqual(['row1', 'row2', 'row3', 'row4', '']);
});

/* Column parsing tests */

test('Parses comma delimited fields', () => {
    let data = `field1, field2, field3, field4`;
    let columns = parser.parseColumns(data);
    expect(columns).toEqual(['field1', 'field2', 'field3', 'field4']);
});

test('Parses pipe delimited fields', () => {
    let data = `field1 | field2 | field3 | field4`;
    let columns = parser.parseColumns(data, '|');
    expect(columns).toEqual(['field1', 'field2', 'field3', 'field4']);
});

test('Parses quoted fields', () => {
    let data = `"field 1", field2, "field 3", field4`;
    let columns = parser.parseColumns(data);
    expect(columns).toEqual(['field 1', 'field2', 'field 3', 'field4']);
});

test('Parses fields with double quotes', () => {
    let data = `"field ""1""", field2, "field ""3""", field4`;
    let columns = parser.parseColumns(data);
    expect(columns).toEqual(['field "1"', 'field2', 'field "3"', 'field4']);
});

test('Parses quoted column delimiters', () => {
    let data = `"field, 1", field2, "field,, 3", field4`;
    let columns = parser.parseColumns(data);
    expect(columns).toEqual(['field, 1', 'field2', 'field,, 3', 'field4']);
});

test('Parses empty fields', () => {
    let data = `field1, field2,, field4`;
    let columns = parser.parseColumns(data);
    expect(columns).toEqual(['field1', 'field2', null, 'field4']);
});

test('Parses fields with empty quotes', () => {
    let data = `field1, field2, "", field4`;
    let columns = parser.parseColumns(data);
    expect(columns).toEqual(['field1', 'field2', '', 'field4']);
});

test('Parses trailing empty field', () => {
    let data = `field1, field2, field3,`;
    let columns = parser.parseColumns(data);
    expect(columns).toEqual(['field1', 'field2', 'field3', null]);
});

test('Parses numeric fields as numbers', () => {
    let data = `1, 1.2, 0.02, 10`;
    let columns = parser.parseColumns(data);
    expect(columns).toEqual([1, 1.2, 0.02, 10]);
});

test('Parses quoted numeric fields as strings', () => {
    let data = `"1", "1.2", "0.02", "10"`;
    let columns = parser.parseColumns(data);
    expect(columns).toEqual(['1', '1.2', '0.02', '10']);
});

/* CSV parsing tests */

test('Parses comma delimited csv', () => {
    let input = stripIndents`
        ID, Name, Age, Favorite Color
        1, Bob, 23, Red
        2, "Doe, John", 99, """Turquoise"""
    `;
    let output = [{
        "ID": 1,
        "Name": "Bob",
        "Age": 23,
        "Favorite Color": "Red"
    }, {
        "ID": 2,
        "Name": "Doe, John",
        "Age": 99,
        "Favorite Color": "\"Turquoise\""
    }];
    expect(parser.parse(input)).toEqual(output);
});

test('Parses pipe delimited csv', () => {
    let input = stripIndents`
        ID| Name| Age| Favorite Color
        1| Bob| 23| Red
        2| "Doe, John"| 99| """Turquoise"""
    `;
    let output = [{
        "ID": 1,
        "Name": "Bob",
        "Age": 23,
        "Favorite Color": "Red"
    }, {
        "ID": 2,
        "Name": "Doe, John",
        "Age": 99,
        "Favorite Color": "\"Turquoise\""
    }];
    expect(parser.parse(input)).toEqual(output);
});

test('Discards trailing newline in non-strict mode', () => {
    let input = `row1\nrow2\n`;
    let parsed = parser.parse(input);
    expect(parsed.length).toBe(1);
});

test('Parses mixed quoted field in non-strict mode', () => {
    let input = stripIndents`
        field1, field2
        "field 1" continued, field2
    `;
    let output = [{
        field1: '"field 1" continued',
        field2: 'field2'
    }];
    expect(parser.parse(input)).toEqual(output);
});

test('Throws error on mixed quoted field in strict mode', () => {
    expect.assertions(1);
    try {
        let data = stripIndents`
            field1, field2
            "field 1" continued, field2
        `;
        let parsed = strict.parse(data);
    } catch (e) {
        expect(e.message).toBe("Improperly quoted field");
    }
});

test('Throws error on column count mismatch', () => {
    expect.assertions(1);
    try {
        let data = stripIndents`
            field1, field2, field3, field4
            field1, field2
        `;
        let parsed = parser.parse(data);
    } catch (e) {
        expect(e.message).toBe("Number of columns does not match header");
    }
});

test('Throws error on mixed delimiter usage', () => {
    expect.assertions(1);
    try {
        let data = `field1|, field2|, field3|, field4`;
        let detected = parser.detect(data);
    } catch (e) {
        expect(e.message).toBe("Mixed usage of comma and pipe delimiters");
    }
});

test('Throws error on unclosed quote', () => {
    expect.assertions(1);
    try {
        let data = `field1, "field2, field3, field4`;
        let columns = parser.parseColumns(data);
    } catch (e) {
        expect(e.message).toBe("Missing closing quote");
    }
});

test('Throws error on non-ascii characters', () => {
    expect.assertions(1);
    try {
        let data = `field1, field2, field3, field\u00e9`;
        let columns = parser.parseColumns(data);
    } catch (e) {
        expect(e.message).toBe("Non-ascii characters detected");
    }
});
