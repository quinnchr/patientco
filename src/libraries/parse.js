class ParsingError extends Error {}

class csv {

    constructor(strict = true) {
        this.QUOTE_CHAR = '"';
        this.NEWLINE = "\n";
        this.strict = strict;
    }

    /* Parses csv formatted text, will attempt to detect the column separator */
    parse(data, delimiter = null) {
        if ( ! this.strict) {
            // remove any trailing newlines
            data = data.trim();
        }

        let parsed = [];
        let rows = this.parseRows(data);
        let header = rows.shift();
        let [guess] = this.detect(header);

        delimiter = delimiter || guess;

        header = this.parseColumns(header, delimiter);
        rows.forEach((row) => {
            let parsedRow = {};
            let parsedColumns = this.parseColumns(row, delimiter);

            if (parsedColumns.length !== header.length) {
                throw new ParsingError('Number of columns does not match header');
            }

            parsedColumns.forEach((val, i) => {
                parsedRow[header[i]] = val;
            });
            parsed.push(parsedRow);
        });

        return parsed;
    }

    /* Attempts to detect the column delimiter and number of columns */
    detect(row) {
        let count = 0;
        let delimiter = ',';
        let comma = this.parseColumns(row, ',');
        let pipe = this.parseColumns(row, '|');

        if (pipe.length > 1 && comma.length === 1) {
            delimiter = '|';
            count = pipe.length;
        }

        if (comma.length > 1 && pipe.length === 1 ) {
            delimiter = ',';
            count = comma.length;
        }

        if (comma.length > 1 && pipe.length > 1) {
            throw new ParsingError('Mixed usage of comma and pipe delimiters');
        }

        return [delimiter, count];
    }

    /* Parses csv formatted text into rows*/
    parseRows(data) {
        return this.parseChunks(data, this.NEWLINE);
    }

    /**
     *  Parses a row into columns, types will be mapped as follows:
     *  (empty) -> null
     *  ""      -> empty string
     *  10      -> int
     *  10.0    -> float
     *  "10.0"  -> string
     */
    parseColumns(data, delimiter = ",") {
        return this.parseChunks(data, delimiter).map(field => {
            // we only allow ascii chars in fields
            if (/[^\u0000-\u007F]+/.test(field)) {
                throw new ParsingError('Non-ascii characters detected');
            }

            // empty fields should be null
            if (field === "") {
                return null;
            }

            field = field.trim();
            let num = Number(field);

            // convert field if numeric, but ignore if quoted
            if ( ! isNaN(num) && ! /^".*"$/.test(field)) {
                return num;
            }

            // throw error on mixed quotes in strict mode
            if (this.strict) {
                let quoted = field.match(/("([^"]|"")*")/);
                
                if (quoted && quoted[0] !== field) {
                    throw new ParsingError('Improperly quoted field');
                }
            }

            // quoted fields should not contain their surrounding quotes in the parsed data
            field = field.replace(/^"(.*)"$/g, '$1');
            // escaped quotes should be converted to normal quotes
            field = field.replace(/""/g, '"');
            return field;
        });
    }

    /* Parses a string into chunks based on the provided delimiter */
    parseChunks(data, delimiter) {
        let chunks = [];
        let currentChunk = "";
        let currentChar = "";
        let quoteOpen = false;
        let quoteCount = 0;

        data = data.split("");
        
        while (data.length > 0) {
            currentChar = data.shift();

            switch (currentChar) {
                case this.QUOTE_CHAR:
                    quoteCount++;

                    if (quoteOpen === false) {
                        if (data[0] === this.QUOTE_CHAR && data[1] !== this.QUOTE_CHAR) {
                            // handle the degenerate case of an empty quote
                            data.shift();
                            currentChunk += this.QUOTE_CHAR;
                        } else {
                            quoteOpen = true;
                        }
                    } else if (data[0] !== this.QUOTE_CHAR && quoteCount % 2 === 1) {
                        // if there's no more quotes in the sequence and there's an odd number of them
                        // then we have a matching closing quote
                        quoteOpen = false;
                    }

                    currentChunk += this.QUOTE_CHAR;
                    break;
                case delimiter:
                    // ignore the delimiter if it's quoted
                    if (quoteOpen === false) {
                        chunks.push(currentChunk);
                        currentChunk = "";
                        break;
                    }
                default:
                    quoteCount = 0;
                    currentChunk += currentChar;
            }
        }

        if (quoteOpen) {
            throw new ParsingError("Missing closing quote");
        }

        chunks.push(currentChunk);
        return chunks;
    }

}

export { csv };
