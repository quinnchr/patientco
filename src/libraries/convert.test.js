import convert from './convert.js';
import { stripIndents } from 'common-tags'

const mock = require('mock-fs');
const cmd = new convert();

beforeEach(() => {
    mock({
        'unreadable.csv': mock.file({ mode: 0 }),
    });
});

afterEach(() => {
    mock.restore();
});

test('Throws error when file does not exist', () => {
    expect.assertions(1);
    return cmd.read('test.csv').catch((error) => {
        expect(error.code).toBe('ENOENT');
    });
});

test('Throws error when file is not readable', () => {
    expect.assertions(1);
    return cmd.read('unreadable.csv').catch((error) => {
        expect(error.code).toBe('EACCES');
    });
});
